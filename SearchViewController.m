//
//  SearchViewController.m
//  LuckyFlight
//
//  Created by Kristel Villalobos on 18/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import "SearchViewController.h"
#import "APIManager.h"
#import <Realm/Realm.h>
#import "Places.h"
#import "Flight.h"
#import "IataCountry.h"
#import "UIView+RNActivityView.h"
#import "Constants.h"
#import "CitiesCell.h"
#import "CityDetailViewController.h"

#define CITIES_CELL_ID @"CitiesCell"

@interface SearchViewController ()<UITableViewDataSource, UITableViewDelegate,UISearchResultsUpdating,UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UISearchController *searchController;
@property (nonatomic, strong) RLMResults<IataCountry *> *iataArray;
@property (nonatomic, strong) RLMResults <Flight *> *flightSortingArray;
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view showActivityView];
    [self registerCustomCell];
    [self registerNotifications];
    [self registerSearchBar];
     self.tableView.backgroundColor =[UIColor colorWithRed:(62.0/255.0) green:(77.0/255.0) blue:(104.0/255.0) alpha:(1.0)];
    UINavigationBar *bar = [self.navigationController navigationBar];
    [bar setTintColor:[UIColor colorWithRed:(62.0/255.0) green:(77.0/255.0) blue:(104.0/255.0) alpha:(1.0)]];
  
    
 self.title = @"Vuelos";
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];

    [self getFlight];
    
}

- (void)registerSearchBar {
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    
    [self.searchController.searchBar setBarTintColor:[UIColor colorWithRed:(62.0/255.0) green:(77.0/255.0) blue:(104.0/255.0) alpha:(1.0)]];
    [self.searchController.searchBar setTintColor:[UIColor whiteColor]];
    
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = searchController.searchBar.text;
    [self getIataDataSearch:searchString];
    [self.tableView reloadData];
}

 
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
     [self getIataFlightCache];
}



-(void) getFlight{
    NSDate* lastDate = [APIManager getLastDateCache];
    NSDate* now = [NSDate date];
    NSDateComponents *components;
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitHour
                                                 fromDate: lastDate toDate: now options: 0];
    NSInteger hour= [components hour];
    if (lastDate == nil){
        
        [self.view showActivityView];
        [APIManager getFlightTest];
        [APIManager updateLastDateCache];
    } else {
        if (hour >= 1) {
            [self.view showActivityView];
            [APIManager getFlightTest];
            [APIManager updateLastDateCache];

        } else [self getIataFlightCache];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)registerCustomCell{
    UINib *nib = [UINib nibWithNibName:CITIES_CELL_ID bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:CITIES_CELL_ID];
}

#pragma -mark NOTIFICATIONS METHODS

-(void)registerNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getfligthData:) name:GET_FLIGHT_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getIataData:) name:GET_IATA_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showErrorMessage) name:ERROR_FOUND_NOTIFICATION object:nil];
}

-(void)removeNotifications{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GET_FLIGHT_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GET_IATA_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ERROR_FOUND_NOTIFICATION object:nil];

    
}

-(void)dealloc{
    [self removeNotifications];
}


#pragma Flight methods

-(void)getfligthData:(NSNotification*)notification{
   
    RLMResults<Flight *> *flight = [[Flight objectsWhere:@"MinPrice<1200"]  sortedResultsUsingProperty:@"MinPrice" ascending:YES] ;
    self.flightSortingArray = flight;
    NSString *str =@"";
    for (NSInteger i=0; i<self.flightSortingArray.count; i++){
        
        Flight *objFlight = self.flightSortingArray[i];
        str =  [NSString stringWithFormat:@"%@,%@",str,objFlight.OutboundLeg.DestinationId.iataCode];

    }
    
    [APIManager getIATAPlacesWithCitiesId:str];
  
}

- (void) updateIataWithMinPrice:(NSNumber*)price iataCode:(NSString*)itata name:(NSString*) name countryCode:(NSString*)countryCode{
    
    RLMRealm *realm = [RLMRealm defaultRealm];

    [realm beginWriteTransaction];
    [IataCountry createOrUpdateInRealm:realm withValue:@{@"code":itata ,@"name":name ,@"country_code":countryCode , @"price":price}];
    [realm commitWriteTransaction];
    
 
}


-(void)getIataData:(NSNotification*)notification{
    
    RLMResults<Flight *> *flight = [[Flight objectsWhere:@"MinPrice<1200"]  sortedResultsUsingProperty:@"MinPrice" ascending:YES] ;
    self.flightSortingArray = flight;
    
    for (NSInteger i=0; i<self.flightSortingArray.count; i++){
        Flight *objFlight = self.flightSortingArray[i];
        [self updateIataWithMinPrice:objFlight.MinPrice
                            iataCode:objFlight.OutboundLeg.DestinationId.iataCode
                                name:objFlight.OutboundLeg.DestinationId.placeName
                         countryCode:objFlight.OutboundLeg.DestinationId.CityId];
    }
    
    RLMResults<IataCountry *> *iata=  [[IataCountry allObjects]  sortedResultsUsingProperty:@"price" ascending:YES] ;
    self.iataArray = iata;
    [self.tableView reloadData];
    [self showSuccessfullyMessage];
}

-(void)getIataFlightCache{
    
        RLMResults<IataCountry *> *iata=  [[IataCountry allObjects]  sortedResultsUsingProperty:@"price" ascending:YES] ;
        self.iataArray = iata;
    
    [self.tableView reloadData];
    [self showSuccessfullyMessage];
    }

    
-(void)getIataDataSearch:(NSString*)name{
    if (name.length>0) {

        NSString *where = [NSString stringWithFormat:@"name CONTAINS '%@'",name];
    
        RLMResults<IataCountry *> *iata=  [[IataCountry objectsWhere:where]  sortedResultsUsingProperty:@"price" ascending:YES] ;
        self.iataArray = iata;

    }
    [self.tableView reloadData];
    [self showSuccessfullyMessage];
}


#pragma -mark LOADING HELPERS

-(void)showErrorMessage{
    [self showResultMessageWithTitle:@"Error found, please try later" imageName:@"error"];
}

-(void)showSuccessfullyMessage{
    [self.view hideActivityViewWithAfterDelay:1];

}

-(void)showResultMessageWithTitle:(NSString*)title imageName:(NSString*)imageName{
  //  [self.view showActivityViewWithLabel:title image:[UIImage imageNamed:imageName]];
    [self.view hideActivityViewWithAfterDelay:1];
}



#pragma -mark TABLE VIEW DELEGATE
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.iataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CitiesCell *cell = [tableView dequeueReusableCellWithIdentifier:CITIES_CELL_ID];
    [cell setupCell:self.iataArray[indexPath.row]];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CityDetailViewController * cityDetailController = [self.storyboard instantiateViewControllerWithIdentifier:@"CityDetailViewController"];
    
    cityDetailController.iataSelected = self.iataArray[indexPath.row];
    IataCountry *iata = self.iataArray[indexPath.row];
    if (iata.imageBase64 != nil) cityDetailController.imageCode = iata.imageBase64;
    [self.navigationController pushViewController:cityDetailController animated:true];
 
    
}

 

@end
