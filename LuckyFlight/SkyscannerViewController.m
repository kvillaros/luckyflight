//
//  SkyscannerViewController.m
//  LuckyFlight
//
//  Created by Kristel Villalobos on 23/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import "SkyscannerViewController.h"
#import "FlightLive.h"
#import "UIView+RNActivityView.h"

#define API_KEY_SHORT @"kr68696561697818"
#define API_URL_REFERAL @"http://partners.api.skyscanner.net/apiservices/referral/v1.0/"


@interface SkyscannerViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;


@end

@implementation SkyscannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   [self.view showActivityView];
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd"];
    

    NSString *params= [NSString stringWithFormat:@"%@%@/%@/%@/%@%@",@"CR/USD/es-ES/",
                       self.flight.OutboundLeg.OriginId.iataCode,
                       self.flight.OutboundLeg.DestinationId.iataCode,
                       [dateformat stringFromDate:self.flight.OutboundLeg.DepartureDate],
                       [dateformat stringFromDate:self.flight.InboundLeg.DepartureDate],@"?apiKey="];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",API_URL_REFERAL,params,API_KEY_SHORT];

    
    NSURL *websiteUrl = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [self.webView loadRequest:urlRequest];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.view hideActivityView];
}

@end
