//
//  IataCountry.m
//  LuckyFlight
//
//  Created by Kristel Villalobos on 25/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import "IataCountry.h"

@implementation IataCountry
+ (NSString *)primaryKey
{
    return @"code";
}

@end
