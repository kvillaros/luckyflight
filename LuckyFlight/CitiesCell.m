//
//  CitiesCell.m
//  LuckyFlight
//
//  Created by Kristel Villalobos on 25/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import "CitiesCell.h"
#import "IataCountry.h"


@interface CitiesCell()

@property (weak, nonatomic) IBOutlet UIImageView *flagImage;
@property (weak, nonatomic) IBOutlet UILabel *CityLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;


@end


@implementation CitiesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setupCell:(IataCountry*)iata{
    
    self.CityLabel.text =  [NSString stringWithFormat:@"%@ - %@",iata.code, iata.name] ;
    self.priceLabel.text = [NSString stringWithFormat:@"$ %@",[iata.price stringValue]];
      self.flagImage.image = [self decodeBase64ToImage:iata.imageBase64];
   
    
    
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    
    if (strEncodeData == nil ){
        return [UIImage imageNamed:@"world"];
    } else {
        NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
        if (data == nil){
            return [UIImage imageNamed:@"plane"];
        } else {
            return [UIImage imageWithData:data];
        }
    }
}


@end
