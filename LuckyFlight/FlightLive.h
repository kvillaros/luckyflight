//
//  FlightLive.h
//  LuckyFlight
//
//  Created by Kristel Villalobos on 26/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "OutboundLeg.h"
#import "InboundLeg.h"

@interface FlightLive : RLMObject
@property  NSString *QuoteId;
@property  NSString *Direct;
@property  NSDate *QuoteDateTime;
@property  NSNumber<RLMInt> *MinPrice;

@property  OutboundLeg *OutboundLeg;
@property  InboundLeg  *InboundLeg;
@end
RLM_ARRAY_TYPE(FlightLive)
