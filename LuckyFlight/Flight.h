//
//  Flight.h
//  LuckyFlight
//
//  Created by Kristel Villalobos on 23/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "OutboundLegIata.h"
#import "InboundLeg.h"

@interface Flight : RLMObject
@property  NSString *QuoteId;
@property  NSString *Direct;
@property  NSDate *QuoteDateTime;
@property  NSNumber<RLMInt> *MinPrice;

@property  OutboundLegIata *OutboundLeg;
@property  InboundLeg  *InboundLeg;
@end
RLM_ARRAY_TYPE(Flight)
