//
//  CityDetailViewController.m
//  LuckyFlight
//
//  Created by Kristel Villalobos on 26/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import "CityDetailViewController.h"
#import <Realm/Realm.h>
#import "FlightDetailCell.h"
#import "UIView+RNActivityView.h"
#import "APIManager.h"
#import "FlightLive.h"
#import "Constants.h"
#import "IataCountry.h"
#import "SkyscannerViewController.h"
#define FLIGHT_DETAIL_CELL_ID @"FlightDetailCell"

@interface CityDetailViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) RLMResults <FlightLive *> *flightCityArray;

@end

@implementation CityDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor =[UIColor colorWithRed:(62.0/255.0) green:(77.0/255.0) blue:(104.0/255.0) alpha:(1.0)];
    [self mainInitializer];
    [self registerNotifications];
    self.title = self.iataSelected.name;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)mainInitializer{
    [self registerCustomCell];
       [self.view showActivityView];
    [APIManager getFlightCityDetail:self.iataSelected.code];
    
}


-(void)registerCustomCell{
    UINib *nib = [UINib nibWithNibName:FLIGHT_DETAIL_CELL_ID bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:FLIGHT_DETAIL_CELL_ID];
}

#pragma -mark NOTIFICATIONS METHODS

-(void)registerNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getfligthData:) name:GET_FLIGHT_LIVE_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showErrorMessage) name:ERROR_FOUND_NOTIFICATION object:nil];
}

-(void)removeNotifications{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GET_FLIGHT_LIVE_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ERROR_FOUND_NOTIFICATION object:nil];
    
    
}

-(void)dealloc{
    [self removeNotifications];
}


#pragma -mark Get Fligth Detail.

-(void)getfligthData:(NSNotification*)notification{
    
    RLMResults<FlightLive *> *flight = [[FlightLive objectsWhere:@"MinPrice<1200"]  sortedResultsUsingProperty:@"MinPrice" ascending:YES] ;
    self.flightCityArray = flight;
    [self showSuccessfullyMessage];
    [self.tableView reloadData];
}


#pragma -mark LOADING HELPERS

-(void)showErrorMessage{
    [self showResultMessageWithTitle:@"Error found, please try later" imageName:@"error"];
}

-(void)showSuccessfullyMessage{
    [self.view hideActivityViewWithAfterDelay:1];
    
}

-(void)showResultMessageWithTitle:(NSString*)title imageName:(NSString*)imageName{
    //  [self.view showActivityViewWithLabel:title image:[UIImage imageNamed:imageName]];
    [self.view hideActivityViewWithAfterDelay:1];
}



#pragma -mark TABLE VIEW DELEGATE
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.flightCityArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FlightDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:FLIGHT_DETAIL_CELL_ID];
    [cell setupCell:self.flightCityArray[indexPath.row] imageCode:self.imageCode];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   SkyscannerViewController * skyViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SkyscannerViewController"];
    
     skyViewController.flight = self.flightCityArray[indexPath.row];
    
   //  sk.dogSelected = self.dogsArray[indexPath.row];
    [self.navigationController pushViewController:skyViewController animated:true];
}


@end
