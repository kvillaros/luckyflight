//
//  FlightDetailCell.m
//  LuckyFlight
//
//  Created by Kristel Villalobos on 23/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import "FlightDetailCell.h"
#import "Flight.h"


@interface FlightDetailCell()

@property (weak, nonatomic) IBOutlet UILabel *destinationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *flagImage;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *datesLabel;
@property (weak, nonatomic) IBOutlet UILabel *iataGo;
@property (weak, nonatomic) IBOutlet UILabel *iataBack;
@property (weak, nonatomic) IBOutlet UIImageView *planeImage;

 

@end


@implementation FlightDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setupCell:(Flight*)flight imageCode:(NSString*)imageCode{
    
    self.destinationLabel.text =  [NSString stringWithFormat:@"%@",flight.InboundLeg.OriginId.CityName] ;
    self.priceLabel.text = [NSString stringWithFormat:@"$%@",[flight.MinPrice stringValue]];
    
    
    NSString *dateString = [NSDateFormatter localizedStringFromDate:flight.InboundLeg.DepartureDate
                                                         dateStyle:NSDateFormatterShortStyle
                                                         timeStyle:NSDateFormatterNoStyle];
    
    
    NSString *dateString2 = [NSDateFormatter localizedStringFromDate:flight.OutboundLeg.DepartureDate
                                                          dateStyle:NSDateFormatterShortStyle
                                                          timeStyle:NSDateFormatterNoStyle];
    self.datesLabel.text = [NSString stringWithFormat:@"%@ - %@",dateString2,dateString];
    self.iataGo.text = [NSString stringWithFormat:@"%@ -> %@",flight.OutboundLeg.OriginId.iataCode,flight.OutboundLeg.DestinationId.iataCode];
    self.iataBack.text = [NSString stringWithFormat:@"%@ <- %@",flight.InboundLeg.OriginId.iataCode,flight.InboundLeg.DestinationId.iataCode];
    
 
        self.flagImage.image =[self decodeBase64ToImage:imageCode];
 
    
    self.planeImage.image = [UIImage imageNamed:@"plane"];
 
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
   
  
    
    if (strEncodeData == nil ){
        return [UIImage imageNamed:@"world"];
    } else {
         NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
        if (data == nil){
            return [UIImage imageNamed:@"icon"];
        } else {
            return [UIImage imageWithData:data];
        }
    }
    
}
@end
