//
//  APIManager.m
//  Translate
//
//  Created by Kristel Villalobos on 10/15/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import "APIManager.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "OutboundLeg.h"
#import "InboundLeg.h"
#import "Flight.h"
#import <Realm/Realm.h>
#import "Places.h"
#import "IataCountry.h"
#import "Carriers.h"
#import "FlightLive.h"
#import "DateCache.h"
#import "OutboundLegIata.h"

@implementation APIManager

#define API_KEY @"kr686965616978188148449541762804"
#define API_URL @"http://partners.api.skyscanner.net/apiservices/browsequotes/v1.0/CR/USD/en-GB/CR/anywhere/anytime/anytime?apiKey="
#define API_CITY_FLIGHT_URL @"http://partners.api.skyscanner.net/apiservices/browsequotes/v1.0/"

#define API_KEY_IATA @"db25db79-5847-4320-b872-97b497cbcf41"
#define API_URL_IATA @"https://iatacodes.org/api/v6/cities?api_key="




+(AFHTTPSessionManager*)createSessionManager{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    return manager;
}

+(AFHTTPSessionManager*)createSessionManagerIata{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    return manager;
}

+ (void) getIATAPlacesWithCitiesId:(NSString*) citiesId {
    
    // Get the diferent places codes.
    NSString *codes_id = [citiesId substringWithRange:NSMakeRange(1, [citiesId length]-1)];

    AFHTTPSessionManager *manager = [APIManager createSessionManagerIata];
    NSString *urlString = [NSString stringWithFormat:@"%@%@&code=%@",API_URL_IATA,API_KEY_IATA,codes_id];
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [APIManager featchIata:[responseObject valueForKey:@"response"]];
        [[NSNotificationCenter defaultCenter] postNotificationName:GET_IATA_NOTIFICATION object:nil userInfo:nil];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [APIManager sendErrorFoundNotification];
        
    }];
    
    
}

+(void) getFlightTest {
    
    
    AFHTTPSessionManager *manager = [APIManager createSessionManager];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",API_URL,API_KEY];
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    
        [APIManager deleteModels];
        [APIManager featchPlaces:[responseObject valueForKey:PLACES_KEY]];
        [APIManager featchFlight:[responseObject valueForKey:QUOTES_KEY]];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:GET_FLIGHT_NOTIFICATION object:nil userInfo:nil];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       [APIManager sendErrorFoundNotification];
    
    }];
    
}
+(void) getFlightCityDetail :(NSString*) IataCode{
   
    
    NSString *params = [NSString stringWithFormat:@"%@/%@/%@",@"CR/USD/en-GB/SJO",IataCode,@"anytime/anytime?apiKey="];
    
    
    AFHTTPSessionManager *manager = [APIManager createSessionManager];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",API_CITY_FLIGHT_URL,params,API_KEY];
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        RLMResults<FlightLive *> *fligthLive = [FlightLive allObjects];
        if (fligthLive.count > 0) {
            
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            [realm deleteObjects:fligthLive];
            [realm commitWriteTransaction];
            
        }
        
        [APIManager featchFlightLive:[responseObject valueForKey:QUOTES_KEY]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:GET_FLIGHT_LIVE_NOTIFICATION object:nil userInfo:nil];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [APIManager sendErrorFoundNotification];
        
    }];
    
}


#pragma mark -deletemodels
+ (void) deleteModels {
    
    RLMResults<Places *> *places = [Places allObjects];
    if (places.count > 0) {
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObjects:places];
        [realm commitWriteTransaction];
        
    }
    
    RLMResults<InboundLeg *> *inboud = [InboundLeg allObjects];
    if (inboud.count > 0) {
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObjects:inboud];
        [realm commitWriteTransaction];
        
    }
    
    
    RLMResults<OutboundLeg *> *outbound = [OutboundLeg allObjects];
    if (outbound.count > 0) {
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObjects:outbound];
        [realm commitWriteTransaction];
        
    }
    RLMResults<OutboundLegIata *> *outboundIata = [OutboundLegIata allObjects];
    if (outbound.count > 0) {
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObjects:outboundIata];
        [realm commitWriteTransaction];
        
    }
    
    RLMResults<Flight *> *fligth = [Flight allObjects];
    if (fligth.count > 0) {
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObjects:fligth];
        [realm commitWriteTransaction];
        
    }
    RLMResults<FlightLive *> *fligthLive = [FlightLive allObjects];
    if (fligthLive.count > 0) {
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObjects:fligthLive];
        [realm commitWriteTransaction];
        
    }
    
    
}



#pragma -mark featch methods


+(void)featchPlaces:(NSArray*)places{
 
   NSDictionary *auxDictionary = [NSDictionary new];
 
    for (int i=0; i<[places count]; i++) {
        auxDictionary = places[i];
        [APIManager fillPlaces:auxDictionary];

    }

}



+(void)featchIata:(NSArray*)IataCities{
    
    RLMResults<IataCountry *> *iata = [IataCountry allObjects];
    if (iata.count > 0) {
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObjects:iata];
        [realm commitWriteTransaction];
        
    }
    
    NSDictionary *auxDictionary = [NSDictionary new];
    
    for (int i=0; i<[IataCities count]; i++) {
        auxDictionary = IataCities[i];
        [APIManager fillIataCities:auxDictionary];
        
    }
}

+(void)featchFlight:(NSArray*)flight{
    
    NSDictionary *auxDictionary = [NSDictionary new];
    
    for (int i=0; i<[flight count]; i++) {
        auxDictionary = flight[i];
        [APIManager fillFlight:auxDictionary];
        
    }
}

+(void)featchFlightLive:(NSArray*)flight{
    
    NSDictionary *auxDictionary = [NSDictionary new];
    
    for (int i=0; i<[flight count]; i++) {
        auxDictionary = flight[i];
        [APIManager fillFlightLive:auxDictionary];
        
    }
}



+(void)featchInbound:(NSArray*)flight{
    
    NSDictionary *auxDictionary = [NSDictionary new];
    
    for (int i=0; i<[flight count]; i++) {
        auxDictionary = flight[i];
        [APIManager fillInbound:auxDictionary];
        
    }
    
}

+(void)featchOutbound:(NSArray*)flight{
    
    NSDictionary *auxDictionary = [NSDictionary new];
    
    for (int i=0; i<[flight count]; i++) {
        auxDictionary = flight[i];
        [APIManager fillOutbound:auxDictionary];
        
    }
    
}
+(void)featchOutboundIata:(NSArray*)flight{
    
    NSDictionary *auxDictionary = [NSDictionary new];
    
    for (int i=0; i<[flight count]; i++) {
        auxDictionary = flight[i];
        [APIManager fillOutboundIata:auxDictionary];
        
    }
    
}

#pragma -mark fill the models

+ (void)fillPlaces:(NSDictionary*)auxplaces{
    
 
         
         // Create the places with the key.
         [APIManager createPlaceWithPlaceId:[NSString stringWithFormat:@"%@", [auxplaces valueForKey:PlaceId_KEY]]
                                                              iataCode:[auxplaces valueForKey:IataCode_KEY]
                                                             placeName:[auxplaces valueForKey:PlaceName_KEY]
                                                        skyscannerCode:[auxplaces valueForKey:SkyscannerCode_KEY]
                                                              cityName:[auxplaces valueForKey:CityName_KEY]
                                                                cityId:[auxplaces valueForKey:CityId_KEY]
                                                           countryName:[auxplaces valueForKey:CountryName_KEY]
                                             ];
         
    
    
}


+(OutboundLegIata*)fillOutboundIata:(NSDictionary*)auxplaces{
    
    
    // getting the place.
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"self.placeId = %@",[NSString stringWithFormat:@"%@",[auxplaces valueForKey:OriginId_KEY]]];
    RLMResults<Places *> *placeorigin = [Places objectsWithPredicate:predicate1];
    
    
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"self.placeId = %@",[NSString stringWithFormat:@"%@",[auxplaces valueForKey:DestinationId_KEY]]];
    RLMResults<Places *> *placedestination = [Places objectsWithPredicate:predicate2];
    
    // date formart
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    OutboundLegIata * Outboundplace = [[OutboundLegIata alloc] init];
    RLMRealm *realm = [RLMRealm defaultRealm];
    Outboundplace.DepartureDate  = [dateformat dateFromString:[auxplaces valueForKey:DepartureDate_KEY]];
    Outboundplace.OriginId = placeorigin.firstObject;
    Outboundplace.CarrierName = @"Avianca";
    Outboundplace.DestinationId = placedestination.firstObject;
    
    [realm beginWriteTransaction];
    [realm addObject:Outboundplace];
    [realm commitWriteTransaction];
    
    return Outboundplace;
    
}
+(OutboundLeg*)fillOutbound:(NSDictionary*)auxplaces{
    
    
    // getting the place.
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"self.placeId = %@",[NSString stringWithFormat:@"%@",[auxplaces valueForKey:OriginId_KEY]]];
    RLMResults<Places *> *placeorigin = [Places objectsWithPredicate:predicate1];
    
    
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"self.placeId = %@",[NSString stringWithFormat:@"%@",[auxplaces valueForKey:DestinationId_KEY]]];
    RLMResults<Places *> *placedestination = [Places objectsWithPredicate:predicate2];
    
    // date formart
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    OutboundLeg * Outboundplace = [[OutboundLeg alloc] init];
    RLMRealm *realm = [RLMRealm defaultRealm];
    Outboundplace.DepartureDate  = [dateformat dateFromString:[auxplaces valueForKey:DepartureDate_KEY]];
    Outboundplace.OriginId = placeorigin.firstObject;
    Outboundplace.CarrierName = @"Avianca";
    Outboundplace.DestinationId = placedestination.firstObject;
    
    [realm beginWriteTransaction];
    [realm addObject:Outboundplace];
    [realm commitWriteTransaction];
    
    return Outboundplace;
    
}

+(InboundLeg*)fillInbound:(NSDictionary*)auxplaces{
    
    
    // getting the place.
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"self.placeId = %@",[NSString stringWithFormat:@"%@",[auxplaces valueForKey:OriginId_KEY]]];
    RLMResults<Places *> *placeorigin = [Places objectsWithPredicate:predicate1];
    
    
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"self.placeId = %@",[NSString stringWithFormat:@"%@",[auxplaces valueForKey:DestinationId_KEY]]];
    RLMResults<Places *> *placedestination = [Places objectsWithPredicate:predicate2];
 
    // date formart
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    InboundLeg * Inboundplace = [[InboundLeg alloc] init];
    RLMRealm *realm = [RLMRealm defaultRealm];
    Inboundplace.DepartureDate  = [dateformat dateFromString:[auxplaces valueForKey:DepartureDate_KEY]];
    Inboundplace.OriginId = placeorigin.firstObject;
    Inboundplace.CarrierName = @"hola";
    Inboundplace.DestinationId = placedestination.firstObject;
 
    [realm beginWriteTransaction];
    [realm addObject:Inboundplace];
    [realm commitWriteTransaction];

    return Inboundplace;
    
}


+ (void)fillIataCities:(NSDictionary*)auxplaces{

    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",@"http://www.geognos.com/api/en/countries/flag/",[auxplaces valueForKey:country_code_KEY],@".png"];
    NSURL* aURL = [NSURL URLWithString:strUrl];
    NSData* data = [[NSData alloc] initWithContentsOfURL:aURL];
    UIImage *imageflag   = nil;
    
    
    if (data == nil)
    {
        imageflag   = [UIImage imageNamed:@"plane"];
        
    }else {
        imageflag   = [UIImage imageWithData:data];
    }
    
    
    // Get the default Realm
    RLMRealm *realm = [RLMRealm defaultRealm];
    IataCountry *iata = [[IataCountry alloc] init];
    if (![[auxplaces valueForKey:name_KEY] isEqual: @""]){
        iata.code = [NSString stringWithFormat:@"%@", [auxplaces valueForKey:code_KEY]];
        iata.name = [NSString stringWithFormat:@"%@", [auxplaces valueForKey:name_KEY]];
        iata.country_code = [NSString stringWithFormat:@"%@", [auxplaces valueForKey:country_code_KEY]];
        iata.price = 0;
        iata.imageBase64 = [APIManager encodeToBase64String:imageflag];
    
        [realm beginWriteTransaction];
        [realm addObject:iata];
        [realm commitWriteTransaction];
    }
    
    
}

+ (void)fillFlightLive:(NSDictionary*)auxplaces{
    
    
    InboundLeg *inboundObj   = [APIManager fillInbound:[auxplaces valueForKey:@"InboundLeg"]];
    OutboundLeg *outboundObj = [APIManager fillOutbound:[auxplaces valueForKey:@"OutboundLeg"]];
    
    
    // date formart
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    
    // Get the default Realm
    RLMRealm *realm = [RLMRealm defaultRealm];
    FlightLive *flight = [[FlightLive alloc] init];
    
    flight.QuoteId = [NSString stringWithFormat:@"%@", [auxplaces valueForKey:QuoteId_KEY]];
    flight.Direct  = [NSString stringWithFormat:@"%@", [auxplaces valueForKey:Direct_KEY]];
    flight.QuoteDateTime = [dateformat dateFromString:[auxplaces valueForKey:QuoteDateTime_KEY]];
    flight.MinPrice = [auxplaces valueForKey:MinPrice_KEY];
    flight.InboundLeg = inboundObj;
    flight.OutboundLeg = outboundObj;
    
    [realm beginWriteTransaction];
    [realm addObject:flight];
    [realm commitWriteTransaction];
    
}


+ (void)fillFlight:(NSDictionary*)auxplaces{
    
    
 //   InboundLeg *inboundObj   = [APIManager fillInbound:[auxplaces valueForKey:@"InboundLeg"]];
    OutboundLegIata *outboundObj = [APIManager fillOutboundIata:[auxplaces valueForKey:@"OutboundLeg"]];
    
    
    // date formart
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
  
        // Get the default Realm
        RLMRealm *realm = [RLMRealm defaultRealm];
        Flight *flight = [[Flight alloc] init];
        
        flight.QuoteId = [NSString stringWithFormat:@"%@", [auxplaces valueForKey:QuoteId_KEY]];
        flight.Direct  = [NSString stringWithFormat:@"%@", [auxplaces valueForKey:Direct_KEY]];
        flight.QuoteDateTime = [dateformat dateFromString:[auxplaces valueForKey:QuoteDateTime_KEY]];
        flight.MinPrice = [auxplaces valueForKey:MinPrice_KEY];
      //  flight.InboundLeg = inboundObj;
        flight.OutboundLeg = outboundObj;
    
        [realm beginWriteTransaction];
        [realm addObject:flight];
        [realm commitWriteTransaction];

}


+ (void) createPlaceWithPlaceId: (NSString*)placeid
                       iataCode:(NSString*) iataCode
                      placeName:(NSString*)placeName
                 skyscannerCode:(NSString*)skyCode
                       cityName:(NSString*)cityName
                         cityId:(NSString*)cityId
                    countryName:(NSString*)countryName{
    // Get the default Realm
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    Places *place = [[Places alloc] init];
    place.placeId = placeid;
    place.iataCode = iataCode;
    place.placeName = placeName;
    place.SkyscannerCode = skyCode;
    place.CityName = cityName;
    place.CityId = cityId;
    place.CountryName = countryName;
    
    
    [realm beginWriteTransaction];
    [realm addObject:place];
    [realm commitWriteTransaction];
}

#pragma -mark Get Cache Data
+(void) updateLastDateCache {
    
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    [DateCache createOrUpdateInRealm:realm withValue:@{@"code":@"1",@"LastDate":[NSDate date]}];
    [realm commitWriteTransaction];
    
    
}
+(NSDate*) getLastDateCache{
    
    RLMResults<DateCache *> *dateCache = [DateCache allObjects];
    NSDate *date =  [dateCache firstObject].LastDate;
    
    return  date;
}



#pragma -mark Notification

+(void)sendErrorFoundNotification{
    [[NSNotificationCenter defaultCenter] postNotificationName:ERROR_FOUND_NOTIFICATION object:nil];
}

#pragma -mark base64
+ (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}





@end
