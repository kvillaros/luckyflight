//
//  APIManager.h
//  Translate
//
//  Created by Cesar Brenes on 10/15/16.
//  Copyright © 2016 Cesar Brenes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIManager : NSObject
+(void) getFlightTest;
+ (void) getIATAPlacesWithCitiesId:(NSString*) citiesId;
+(void) getFlightCityDetail :(NSString*) IataCode;
+(void) updateLastDateCache;
+(NSDate*) getLastDateCache; 
 
@end
