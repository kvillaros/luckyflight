//
//  AppDelegate.h
//  LuckyFlight
//
//  Created by Kristel Villalobos on 11/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

