//
//  Places.h
//  LuckyFlight
//
//  Created by Kristel Villalobos on 18/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface Places : RLMObject
@property  NSString *placeId;
@property  NSString *iataCode;
@property  NSString *placeName;
@property  NSString *SkyscannerCode;
@property  NSString *CityName;
@property  NSString *CityId;
@property  NSString *CountryName;



@end
RLM_ARRAY_TYPE(Places)
