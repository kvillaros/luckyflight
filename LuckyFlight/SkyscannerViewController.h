//
//  SkyscannerViewController.h
//  LuckyFlight
//
//  Created by Kristel Villalobos on 23/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FlightLive;
@interface SkyscannerViewController : UIViewController
@property (nonatomic,strong) FlightLive *flight;
@end
