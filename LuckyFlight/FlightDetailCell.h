//
//  FlightDetailCell.h
//  LuckyFlight
//
//  Created by Kristel Villalobos on 23/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Flight;
@interface FlightDetailCell : UITableViewCell
-(void)setupCell:(Flight*)flight imageCode:(NSString*)imageCode;
@end
