//
//  Carriers.h
//  LuckyFlight
//
//  Created by Kristel Villalobos on 23/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
@interface Carriers : RLMObject
@property  NSString *CarrierId;
@property  NSString *Name;
@end
