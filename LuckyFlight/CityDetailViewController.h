//
//  CityDetailViewController.h
//  LuckyFlight
//
//  Created by Kristel Villalobos on 26/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IataCountry;

@interface CityDetailViewController : UIViewController
@property (nonatomic,strong) IataCountry  *iataSelected;
@property (nonatomic,strong) NSString  *imageCode;
@end
