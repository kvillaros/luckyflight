//
//  OutboundLegIata.h
//  LuckyFlight
//
//  Created by Kristel Villalobos on 28/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "Places.h"
@interface OutboundLegIata : RLMObject
@property  NSDate *DepartureDate;
@property  Places *OriginId;
@property  NSString *CarrierName;
@property  Places *DestinationId;


@end
RLM_ARRAY_TYPE(OutboundLegIata)
