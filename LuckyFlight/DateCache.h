//
//  DateCache.h
//  LuckyFlight
//
//  Created by Kristel Villalobos on 27/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface DateCache : RLMObject
@property  NSString *code;
@property  NSDate *LastDate;
@end
RLM_ARRAY_TYPE(DateCache)
