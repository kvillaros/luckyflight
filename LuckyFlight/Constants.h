//
//  Constants.h
//  LuckyFlight
//
//  Created by Kristel Villalobos on 22/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#ifndef Constants_h
#define Constants_h
#define PLACES_KEY @"Places"
#define QUOTES_KEY @"Quotes"
#define CARRIERS_KEY @"Carriers"
#define CURRENCIES_KEY @"Currencies"
#define PLACES_ARRAY_KEY  @"LENGUAGES_ARRAY"


#pragma -mark THE KEYS FOR PLACES TABLE
#define CityId_KEY @"CityId"
#define CityName_KEY @"CityName"
#define CountryName_KEY @"CountryName"
#define IataCode_KEY @"IataCode"
#define PlaceName_KEY @"Name"
#define PlaceId_KEY @"PlaceId"
#define SkyscannerCode_KEY @"SkyscannerCode"
#define Type_KEY @"Type"

#pragma -mark THE KEYS FOR FLIGTH TABLE
#define Direct_KEY @"Direct"
#define InboundLeg_KEY @"InboundLeg"
#define MinPrice_KEY @"MinPrice"
#define QuoteDateTime_KEY @"QuoteDateTime"
#define QuoteId_KEY @"QuoteId"

#pragma -mark THE KEYS FOR INBOUNDLEG Y OUTBOUNDLEG TABLE
#define CarrierIds_KEY @"CarrierIds"
#define DepartureDate_KEY @"DepartureDate"
#define DestinationId_KEY @"DestinationId"
#define OriginId_KEY @"OriginId"


#pragma -mark THE KEYS FOR IATA TABLE
#define code_KEY @"code"
#define name_KEY @"name"
#define country_code_KEY @"country_code"

#pragma -mark THE KEYS FOR CARRIES TABLE
#define CarrierId_KEY @"CarrierId"
#define CarrierName_KEY @"Name"





#define GET_FLIGHT_NOTIFICATION @"GET_FLIGHT_NOTIFICATION"
#define GET_FLIGHT_LIVE_NOTIFICATION @"GET_FLIGHT_LIVE_NOTIFICATION"
#define GET_IATA_NOTIFICATION @"GET_IATA_NOTIFICATION"
#define ERROR_FOUND_NOTIFICATION @"ERROR_FOUND_NOTIFICATION"
#define GET_TRANSLATE_NOTIFICATION @"GET_TRANSLATE_NOTIFICATION"
#define TRANSLATE_RESULT_KEY @"TRANSLATE_RESULT_KEY"
#endif /* Constants_h */
