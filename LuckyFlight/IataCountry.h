//
//  IataCountry.h
//  LuckyFlight
//
//  Created by Kristel Villalobos on 25/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Realm/Realm.h>

@interface IataCountry : RLMObject
@property  NSString *code;
@property  NSString *name;
@property  NSString *country_code;
@property  NSNumber<RLMInt> *price;
@property  NSString *imageBase64;
@end
RLM_ARRAY_TYPE(IataCountry)

