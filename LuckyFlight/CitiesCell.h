//
//  CitiesCell.h
//  LuckyFlight
//
//  Created by Kristel Villalobos on 25/10/16.
//  Copyright © 2016 Kristel Villalobos. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IataCountry;

@interface CitiesCell : UITableViewCell
-(void)setupCell:(IataCountry*)iata;
@end
